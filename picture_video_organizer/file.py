# -*- coding: utf-8 -*-
"""
Specifies a file (picture or video) to be processed.

.. module:: file
    :synopsis: module defining a file containing files to be processed.

.. moduleauthor:: Patrick Kennedy <patkennedy79@gmail.com>
"""
from os.path import isfile, isdir
from os import mkdir
from shutil import copy2
from string import replace
import exifread
from datetime import datetime
from dateutil import tz
from hachoir_core.error import HachoirError
from hachoir_core.cmd_line import unicodeFilename
from hachoir_parser import createParser
from hachoir_metadata import extractMetadata


class File(object):
    """Defines a file to be copied to a destination directory.

    This class defines a file that will be copied from its source directory to a
    destination directory, if the file does not exist in the destination directory already.

    The constructor checks that the path for the source directory and the path for the two
    destination directories (pictures and videos) are valid.  Next, a check is performed to
    see if the directory containing this file already contains the date, as this is used
    as the date associated with this file.  Finally, the parameters about the copy operation
    for this class are initialized to zeros.

    :param filename: name of the file with its extension, but without the directory
    :param source_directory: directory where the file exists
    :param destination_directory: directory where the file will be attempted to be
                                 copied to
    :param file_size: size (in Bytes) of the file
    :param date_created: date (in format of "xx-yy-zz", such as "12-23-15" for
                         December 23rd, 2015)
    :param copy_successful: flag indicating if the copy from the source directory
                            to the destination directory was successful
    """

    def __init__(self, filename, source_directory, destination_directory):
        """Initialize the parameters for the file."""
        self.filename = filename
        self.source_directory = File.check_directory_name(source_directory)
        self.destination_directory = File.check_directory_name(destination_directory)
        self.file_size = 0
        self.copy_successful = False
        self.date_created = ""

        # Check if the creation date is already included
        print self.source_directory
        directories = self.source_directory.split('/')
        directories = [dir for dir in directories if dir != '']
        print directories
        if directories[-1].startswith('20'):
            self.date_created = directories[-1]
            self.date_created = replace(self.date_created, "_", "-")
        else:
            self.date_created = ""

    def print_details(self):
        """Print the details about the source directory, including the copy results."""
        print "File details:"
        print "    Filename: %s" % self.filename
        print "    Source directory: %s" % self.source_directory
        print "    Destination directory: %s" % self.destination_directory
        print "    Date created: %s" % self.date_created
        print "    Copy successful: %s" % self.copy_successful

    def get_date_found(self):
        """Indicate if the date associated with the file has been identified."""
        return not (self.date_created == "")

    @staticmethod
    def check_directory_name(directory_name):
        """Check that the specified directory ends with a forward-slash."""
        if not directory_name.endswith('/'):
            return directory_name + '/'
        else:
            return directory_name

    def copy_to_destination_directory(self):
        """Copy the file from the source directory to the destination directories (picture or video).

        First, this method first checks if the destination directory exists.  If it does not, then the
        destination directory is created.

        Second, this method checks if the file exists in the destination directory.  If it does not,
        then the file is copied to the destination directory.

        """
        if not isdir(self.destination_directory):
            print "Destination directory does NOT exist!"
            try:
                mkdir(self.destination_directory)
            except OSError as e:
                print "Mkdir Exception: %s" % str(e)

        if not isfile(self.destination_directory + self.filename):
            print "File is NOT located in the destination directory!"
            print "Copying %s to %s" % (self.filename, self.destination_directory)
            try:
                copy2((self.source_directory + self.filename), self.destination_directory)
                self.copy_successful = True
            except OSError as e:
                print "Copy2 Exception: %s" % str(e)
        else:
            print "File exists in the destination directory."


class PictureFile(File):
    """Defines a picture file to be copied to a destination directory.

    This class defines a picture file, as distinguished by the "*.jpg" file extension.

    The constructor starts by calling the constructor of the :class:`File <picture_video_organizer.file.File>`
    class.  Next, the metadata of the file is processed to extract the date that the file was created.

    This class inherits from the :class:`File <picture_video_organizer.file.File>` class.

    """

    def __init__(self, filename, source_directory, destination_directory):
        """Initialize the parameters for the picture file."""
        super(PictureFile, self).__init__(filename, source_directory, destination_directory)

        if self.date_created == "":
            # Open the picture file for reading the meta data (binary mode)
            f = open((self.source_directory + self.filename), 'rb')

            # Return Exif tags
            exif_tags = exifread.process_file(f)

            for tag in exif_tags.keys():
                print "Current tag: %s" % tag
                if tag == 'EXIF DateTimeOriginal':
                    date_file = str(exif_tags[tag])
                    photo_date = date_file[0:10]
                    self.date_created = str.replace(photo_date, ":", "-")
                    self.destination_directory += self.date_created + '/'
                    print "Picture created on %s" % self.date_created
                    print "Picture dest dir: %s" % self.destination_directory

            if self.date_created == "":
                print "***EXIF Date Created not found!!!***"
                self.destination_directory += 'no_date_available' + '/'
        else:
            self.destination_directory += self.date_created + '/'


class VideoFile(File):
    """Defines a video file to be copied to a destination directory.

    This class defines a video file, as distinguished by the "*.mov" or "*.mp4" file extension.

    The constructor starts by calling the constructor of the :class:`File <picture_video_organizer.file.File>`
    class.  Next, the metadata of the file is processed to extract the date that the file was created.

    This class inherits from the :class:`File <picture_video_organizer.file.File>` class.

    """

    def __init__(self, filename, source_directory, destination_directory):
        """Initialize the parameters for the picture file."""
        super(VideoFile, self).__init__(filename, source_directory, destination_directory)

        if self.date_created == "":
            # Set the timezone data for processing the movie files
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('America/Los_Angeles')

            file_with_path = self.source_directory + "/" + self.filename
            filename, realname = unicodeFilename(file_with_path), file_with_path
            parser = createParser(filename, realname)

            if not parser:
                print "ERROR... unable to parse file!"
            else:
                try:
                    metadata = extractMetadata(parser)
                # except (HachoirError, err):
                except (HachoirError):
                    # print "Metadata extraction error: %s" % unicode(err)
                    print 'Metadata extraction error:'
                    metadata = None

            if not metadata:
                print "Unable to extract metadata"
            else:
                text = metadata.exportPlaintext()

                for line in text:
                    print line
                    current_line = str(line)[2:15]
                    movie_creation_date_and_time_utc = str(line)[17:len(line)]

                    if current_line == "Creation date":
                        print "Current line: %s" % current_line
                        print "Found match... %s" % movie_creation_date_and_time_utc

                        # Process the time extracted from the movie file by converting from
                        #  UTC time (Greenwich Mean Time) to the Pacific time zone
                        utc = datetime.strptime(movie_creation_date_and_time_utc, '%Y-%m-%d %H:%M:%S')
                        utc = utc.replace(tzinfo=from_zone)
                        movie_creation_date_and_time_pacific = utc.astimezone(to_zone)
                        print "Time/Date: %s" % movie_creation_date_and_time_pacific

                        # Extract the date from the processed Pacific time
                        movie_creation_date = str(movie_creation_date_and_time_pacific)[0:10]
                        self.date_created = movie_creation_date
                        self.destination_directory += self.date_created + '/'
                        print "Video created on: %s" % self.date_created
                        print "Video dest dir: %s" % self.destination_directory

        else:
            self.destination_directory += self.date_created + '/'
